﻿namespace FCMXamarin
{
    public interface INotification
    {
        void CreateNotification(string title, string message);
    }
}