using System;
using Android.App;
using Android.Content;
using Android.Support.V4.App;
using FCMXamarin.Android;
using Xamarin.Forms;
using Application = Android.App.Application;

[assembly: Dependency(typeof(NotificationHelper))]
namespace FCMXamarin.Android
{
    public class NotificationHelper : INotification
    {
        private readonly Context _context;

        public const string NOTIFICATION_CHANNEL_ID = "100";

        public NotificationHelper()
        {
            _context = Application.Context;
        }

        public void CreateNotification(string title, string message)
        {
            try
            {
                SendNotification(title, message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void SendNotification(string title, string message)
        {
            var notificationBuilder = new NotificationCompat.Builder(_context);
            notificationBuilder
                .SetContentTitle(title)
                .SetAutoCancel(true)
                .SetContentText(message)
                .SetChannelId(NOTIFICATION_CHANNEL_ID)
                .SetPriority((int) NotificationPriority.High)
                .SetVibrate(new long[0])
                .SetSmallIcon(Resource.Drawable.icon)
                .SetDefaults((int) NotificationDefaults.Sound | (int) NotificationDefaults.Vibrate);

            var notificationManager = (NotificationManager) _context.GetSystemService(Context.NotificationService);
            if (global::Android.OS.Build.VERSION.SdkInt >= global::Android.OS.BuildVersionCodes.O)
            {
                const NotificationImportance importance = NotificationImportance.High;
                var channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, title, importance);

                channel.EnableLights(true);
                channel.EnableVibration(true);
                channel.SetShowBadge(true);
                channel.Importance = importance;
                channel.SetVibrationPattern(new long[] {100, 200, 300, 400, 500, 400, 300, 200, 400});

                notificationBuilder.SetChannelId(NOTIFICATION_CHANNEL_ID);
                notificationManager.CreateNotificationChannel(channel);
            }

            notificationManager.Notify(0, notificationBuilder.Build());
        }
    }
}