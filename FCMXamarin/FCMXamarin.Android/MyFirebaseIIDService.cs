﻿using Android.App;
using Firebase.Iid;
using Firebase.Messaging;

namespace FCMXamarin.Android
{
    [Service]
    [IntentFilter(new[] {"com.google.firebase.INSTANCE_ID_EVENT"})]
    public class MyFirebaseIIDService : FirebaseMessagingService
    {
        private const string TAG = "MyFirebaseIIDService";
        public override void OnNewToken(string p0)
        {
            base.OnNewToken(p0);
            
        }
    }
}