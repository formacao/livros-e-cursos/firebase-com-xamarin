﻿using FCMXamarin.iOS;
using UserNotifications;
using Xamarin.Forms;

[assembly: Dependency(typeof(NotificationHelper))]
namespace FCMXamarin.iOS
{
    public class NotificationHelper : INotification
    {
        private bool _aprovou;
        public NotificationHelper()
        {
            // request the permission to use local notifications
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) =>
            {
                _aprovou = approved;
            });
        }
        public void CreateNotification(string title, string message)
        {
            if (!_aprovou) return;
            new NotificationDelegate().RegisterNotification(title, message);
        }
    }
}