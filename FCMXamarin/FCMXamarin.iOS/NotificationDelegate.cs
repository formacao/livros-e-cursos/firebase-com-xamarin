﻿using System;
using Foundation;
using UserNotifications;

namespace FCMXamarin.iOS
{
    public class NotificationDelegate : UNUserNotificationCenterDelegate
    {
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            Console.WriteLine("Active Notification: {0}", notification);
            completionHandler(UNNotificationPresentationOptions.Alert | UNNotificationPresentationOptions.Sound);
        }

        public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            // Take action based on Action ID
            switch (response.ActionIdentifier)
            {
                case "reply":
                    break;
                default:
                    if (response.IsDefaultAction)
                    {
                    }
                    else if (response.IsDismissAction)
                    {
                    }
                    break;
            }

            completionHandler();
        }
        
        public void RegisterNotification(string title, string body)
        {
            var center = UNUserNotificationCenter.Current;

            //creat a UNMutableNotificationContent which contains your notification content
            var notificationContent = new UNMutableNotificationContent();

            notificationContent.Title = title;
            notificationContent.Body = body;

            notificationContent.Sound = UNNotificationSound.Default;

            var trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(1, false);

            var request = UNNotificationRequest.FromIdentifier("FiveSecond", notificationContent, trigger);


            center.AddNotificationRequest(request, (NSError error) =>
            {
                if (error != null)
                {
                    throw new Exception($"Failed to schedule notification: {error}");
                }
            });
        }
    }
}